﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace APPKinect
{
    public class Mapeamento
    {
       
        public void RenderizarEsqueleto(Skeleton skeleton)
        {
            var jointCollection = skeleton.Joints;
            // Render Head and Shoulders
            DrawBone(jointCollection[JointType.Head], jointCollection[JointType.ShoulderCenter]);
            DrawBone(jointCollection[JointType.ShoulderCenter], jointCollection[JointType.ShoulderLeft]);
            DrawBone(jointCollection[JointType.ShoulderCenter], jointCollection[JointType.ShoulderRight]);

            // Render Left Arm
            DrawBone(jointCollection[JointType.ShoulderLeft], jointCollection[JointType.ElbowLeft]);
            DrawBone(jointCollection[JointType.ElbowLeft], jointCollection[JointType.WristLeft]);
            DrawBone(jointCollection[JointType.WristLeft], jointCollection[JointType.HandLeft]);

            // Render Right Arm
            DrawBone(jointCollection[JointType.ShoulderRight], jointCollection[JointType.ElbowRight]);
            DrawBone(jointCollection[JointType.ElbowRight], jointCollection[JointType.WristRight]);
            DrawBone(jointCollection[JointType.WristRight], jointCollection[JointType.HandRight]);

            // Render other bones...
            DrawBone(jointCollection[JointType.Spine], jointCollection[JointType.Spine]);
        }

        private void DrawBone(Joint jointFrom, Joint jointTo)
        {
            if (jointFrom.TrackingState == JointTrackingState.NotTracked ||
            jointTo.TrackingState == JointTrackingState.NotTracked)
            {
                return; // nothing to draw, one of the joints is not tracked
            }

            if (jointFrom.TrackingState == JointTrackingState.Inferred ||
            jointTo.TrackingState == JointTrackingState.Inferred)
            {
                DrawNonTrackedBoneLine(jointFrom.Position, jointTo.Position);  // Draw thin lines if either one of the joints is inferred
            }

            if (jointFrom.TrackingState == JointTrackingState.Tracked &&
            jointTo.TrackingState == JointTrackingState.Tracked)
            {
                DrawTrackedBoneLine(jointFrom.Position, jointTo.Position);  // Draw bold lines if the joints are both tracked
            }
        }

        private void DrawTrackedBoneLine(SkeletonPoint skeletonPoint1, SkeletonPoint skeletonPoint2)
        {
            DrawNonTrackedBoneLine(skeletonPoint1, skeletonPoint2);
        }

        private void DrawNonTrackedBoneLine(SkeletonPoint skeletonPoint1, SkeletonPoint skeletonPoint2)
        {
            var pen = new Pen(System.Windows.Media.Brushes.Red, 5.0);
            
            var drawingContext = new DrawingVisual().RenderOpen();

            var point1 = new Point(skeletonPoint1.X, skeletonPoint1.Y);
            var point2 = new Point(skeletonPoint2.X, skeletonPoint2.Y);      
            drawingContext.DrawLine(pen, point1, point2);
            drawingContext.Close();

        }
    }
}
