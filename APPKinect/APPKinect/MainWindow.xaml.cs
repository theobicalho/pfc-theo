﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace APPKinect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        KinectSensor myKinectSensor;
        Skeleton[] skeletonData;        

        //relativo ao traçado do esqueleto
        private readonly Pen inferredBonePen = new Pen(Brushes.Green, 5);
        private readonly Pen trackedBonePen = new Pen(Brushes.Red, 5);

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            //Verifica se há kinects conectados
            if (KinectSensor.KinectSensors.Count != 0)
            {
                //Verifica se está conectado o primeiro
                if (KinectSensor.KinectSensors[0].Status == KinectStatus.Connected)
                {
                    myKinectSensor = KinectSensor.KinectSensors[0];
                    //Ativa mapeamento de junta
                    myKinectSensor.SkeletonStream.Enable();
                    skeletonData = new Skeleton[myKinectSensor.SkeletonStream.FrameSkeletonArrayLength]; // Allocate ST data

                    myKinectSensor.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(myKinectSensor_SkeletonFrameReady); // Get Ready for Skeleton Ready Events
                    
                    //verificar necessidade
                    myKinectSensor.ColorStream.Enable();
                    myKinectSensor.DepthStream.Enable();
                }
                else
                    MessageBox.Show("O Sensor Kinect ainda não está pronto. Aguarde");
            }
            else
                MessageBox.Show("Verifique se o sensor Kinect está corretamente conectado.");

            PreparaTelaSupervisorio();
            myKinectSensor.Start();
            myKinectSensor.ElevationAngle = 0;
        }

        private void PreparaTelaSupervisorio()
        {
            //Obtenção de dados via sensor (no caso, habilitação da câmera RGB)
            myKinectSensor.ColorFrameReady += myKinectSensor_ColorFrameReady;
            myKinectSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
        }
                
        private void myKinectSensor_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame es = e.OpenColorImageFrame())
            {
                if (!(es == null))
                {
                    byte[] bits = new byte[es.PixelDataLength];
                    es.CopyPixelDataTo(bits);
                    frameCamera.Source = BitmapSource.Create(
                    es.Width, es.Height, 96, 96, PixelFormats.Bgr32, null, bits, es.Width * es.BytesPerPixel);
                }
            }
        }

        private void myKinectSensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame()) // Open the Skeleton frame
            {
                if (skeletonFrame != null && this.skeletonData != null) // check that a frame is available
                {
                    skeletonFrame.CopySkeletonDataTo(this.skeletonData); // get the skeletal information in this frame
                    DrawSkeletons();
                }
            }
        }               

        private void DrawSkeletons()
        {
            foreach (Skeleton skeleton in this.skeletonData)
            {
                if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
                {
                    //deetectou movimento. inserir gcm
                    new Mapeamento().RenderizarEsqueleto(skeleton);
                    //RenderizarEsqueleto(skeleton);
                }
                else if (skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                {
                    //DrawSkeletonPosition(skeleton.Position);
                }
            }
        }


        private void btnAumentarInclinacao_Click(object sender, RoutedEventArgs e)
        {
            if (sliderInclinacao.Value < 50)
                sliderInclinacao.Value += 5;
        }

        private void btnDiminuirInclinacao_Click(object sender, RoutedEventArgs e)
        {
            if (sliderInclinacao.Value >= 5)
                sliderInclinacao.Value -= 5;
        }

        private void btnResetarPosicao_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                myKinectSensor.ElevationAngle = 0;
                sliderInclinacao.Value = 25;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        private void sliderInclinacao_Initialized_1(object sender, EventArgs e)
        {
            sliderInclinacao.Value = 25;
        }

        private void sliderInclinacao_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                myKinectSensor.ElevationAngle = (int)sliderInclinacao.Value - 25;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Fora de limite" + ex.GetBaseException().ToString());
            }
        }

        //private void RenderizarEsqueleto(Skeleton skeleton)
        //{
        //    var jointCollection = skeleton.Joints;
        //    // Render Head and Shoulders
        //    DrawBone(jointCollection[JointType.Head], jointCollection[JointType.ShoulderCenter]);
        //    DrawBone(jointCollection[JointType.ShoulderCenter], jointCollection[JointType.ShoulderLeft]);
        //    DrawBone(jointCollection[JointType.ShoulderCenter], jointCollection[JointType.ShoulderRight]);

        //    // Render Left Arm
        //    DrawBone(jointCollection[JointType.ShoulderLeft], jointCollection[JointType.ElbowLeft]);
        //    DrawBone(jointCollection[JointType.ElbowLeft], jointCollection[JointType.WristLeft]);
        //    DrawBone(jointCollection[JointType.WristLeft], jointCollection[JointType.HandLeft]);

        //    // Render Right Arm
        //    DrawBone(jointCollection[JointType.ShoulderRight], jointCollection[JointType.ElbowRight]);
        //    DrawBone(jointCollection[JointType.ElbowRight], jointCollection[JointType.WristRight]);
        //    DrawBone(jointCollection[JointType.WristRight], jointCollection[JointType.HandRight]);

        //    // Render other bones...
        //    DrawBone(jointCollection[JointType.Spine], jointCollection[JointType.Spine]);
        //}

        //private void DrawBone(Joint jointFrom, Joint jointTo)
        //{
        //    if (jointFrom.TrackingState == JointTrackingState.NotTracked ||
        //    jointTo.TrackingState == JointTrackingState.NotTracked)
        //    {
        //        return; // nothing to draw, one of the joints is not tracked
        //    }

        //    if (jointFrom.TrackingState == JointTrackingState.Inferred ||
        //    jointTo.TrackingState == JointTrackingState.Inferred)
        //    {
        //        DrawNonTrackedBoneLine(jointFrom.Position, jointTo.Position);  // Draw thin lines if either one of the joints is inferred
        //    }

        //    if (jointFrom.TrackingState == JointTrackingState.Tracked &&
        //    jointTo.TrackingState == JointTrackingState.Tracked)
        //    {
        //        DrawTrackedBoneLine(jointFrom.Position, jointTo.Position);  // Draw bold lines if the joints are both tracked
        //    }
        //}

        //private void DrawTrackedBoneLine(SkeletonPoint skeletonPoint1, SkeletonPoint skeletonPoint2)
        //{
        //    DrawNonTrackedBoneLine(skeletonPoint1, skeletonPoint2);
        //}

        //private void DrawNonTrackedBoneLine(SkeletonPoint skeletonPoint1, SkeletonPoint skeletonPoint2)
        //{
        //    Pen pen = this.trackedBonePen;
        //    pen.Thickness = 30;

        //    var drawingContext = new DrawingVisual().RenderOpen();
        //    var point1 = new Point(skeletonPoint1.X, skeletonPoint1.Y);
        //    var point2 = new Point(skeletonPoint2.X, skeletonPoint2.Y);
        //    drawingContext.DrawLine(pen, point1, point2);
            
           
        //}

        //Método de controle do campo de visão
        //private void RenderClippedEdges(Skeleton skeleton)
        //{
        //    if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
        //    {
        //        DrawClippedEdges(FrameEdges.Bottom); // Make the border red to show the user is reaching the border
        //    }

        //    if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
        //    {
        //        DrawClippedEdges(FrameEdges.Top);
        //    }

        //    if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
        //    {
        //        DrawClippedEdges(FrameEdges.Left);
        //    }

        //    if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
        //    {
        //        DrawClippedEdges(FrameEdges.Right);
        //    }
        //}

    }
}
