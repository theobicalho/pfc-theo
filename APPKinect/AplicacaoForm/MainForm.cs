﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Kinect;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using AplicacaoForm.Properties;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Data.SqlClient;

namespace WinFormsKinectCameraViewer
{
    public partial class MainForm : Form
    {
        KinectSensor kinectSensor = null;

        public MainForm()
        {
            InitializeComponent();

            RenderizarTela();
        }

        SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\v11.0");

        protected override void OnShown(EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
        }

        private void RenderizarTela()
        {
            foreach (KinectSensor sensor in KinectSensor.KinectSensors)
            {
                if (sensor.Status == KinectStatus.Connected)
                {
                    sensor.ColorStream.Enable();
                    CriarPainelCompletoSensor(sensor); 
                }
            }
        }
        
        private void AtivarRastreamento(KinectSensor sensor)
        {
            sensor.SkeletonStream.Enable();
        }

        private void DesativarRastreamento(KinectSensor sensor)
        {
            sensor.SkeletonStream.Disable();
        }

        private void SetupSensorVideoInput(KinectSensor sensor, ColorImageFormat colorImageFormat)
        {
            if (sensor != null)
            {
                sensor.ColorStream.Enable(colorImageFormat);
                sensor.ColorFrameReady += kinectSensor_ColorFrameReady;
                sensor.Start();
            }
        }

        private void kinectSensor_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            PictureBox frameCamera = (PictureBox)this.Controls.Find(String.Concat("pb", kinectSensor.UniqueKinectId.ToString()), true).First();
            using (ColorImageFrame es = e.OpenColorImageFrame())
            {
                if (!(es == null))
                {
                    byte[] bits = new byte[es.PixelDataLength];
                    es.CopyPixelDataTo(bits);
                    BitmapSource bmpSource = BitmapSource.Create(
                    es.Width, es.Height, 96, 96, PixelFormats.Bgr32, null, bits, es.Width * es.BytesPerPixel);
                    frameCamera.Image = BitmapFromSource(bmpSource);
                }
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            EncerrarSensores();
        }

        private void EncerrarSensores()
        {
            foreach (KinectSensor sensor in KinectSensor.KinectSensors)
            {
                if (sensor != null)
                {
                    sensor.Stop();
                    sensor.ColorFrameReady -= kinectSensor_ColorFrameReady;
                    sensor.Dispose();
                }
            }
        }

        private void CriarPainelCompletoSensor(KinectSensor sensor)
        {
            //tabela de cada quarto
            TableLayoutPanel tablePanel = new TableLayoutPanel();
            tablePanel.RowCount = 1;
            tablePanel.ColumnCount = 2;
           // tablePanel.Name = sensor.UniqueKinectId;
            tablePanel.MinimumSize = new Size(600, 400);
            tablePanel.AutoSize = true;
            tablePanel.BackColor = System.Drawing.Color.LightGray;
            
            
            //imagem da camera
            PictureBox pbCamera = new PictureBox();
            pbCamera.MinimumSize = new Size(383, 295);
            //pbCamera.Dock = DockStyle.Fill;
            pbCamera.Name = String.Concat("pb",sensor.UniqueKinectId.ToString());

            GroupBox gbPainelCtrl = new GroupBox();
            gbPainelCtrl.Dock = DockStyle.Fill;
            gbPainelCtrl.Text = "Painel de Controle";
            gbPainelCtrl.MinimumSize = new Size(80, 160);
            gbPainelCtrl.BackColor = System.Drawing.Color.DarkGray;
            gbPainelCtrl.ForeColor = System.Drawing.Color.WhiteSmoke;

            tablePanel.Controls.Add(pbCamera, 0, 0);     
            tablePanel.Controls.Add(gbPainelCtrl, 1, 0);

            TableLayoutPanel tableGroupboxesInclinacaoFuncionalidades = new TableLayoutPanel();
            tableGroupboxesInclinacaoFuncionalidades.RowCount = 1;
            tableGroupboxesInclinacaoFuncionalidades.ColumnCount = 2;
            tableGroupboxesInclinacaoFuncionalidades.Dock = DockStyle.Fill;
            tableGroupboxesInclinacaoFuncionalidades.MinimumSize = new Size(130, 180);

            gbPainelCtrl.Controls.Add(tableGroupboxesInclinacaoFuncionalidades);

            GroupBox gbInclinacao = new GroupBox();
            gbInclinacao.Text = "Inclinação";
            gbInclinacao.Dock = DockStyle.Fill;
            gbInclinacao.MaximumSize = new Size(70, 190);

            tableGroupboxesInclinacaoFuncionalidades.Controls.Add(gbInclinacao, 0, 0);

            FlowLayoutPanel panelBotoesInclinacao = new FlowLayoutPanel();
            panelBotoesInclinacao.FlowDirection = FlowDirection.TopDown;
            panelBotoesInclinacao.Dock = DockStyle.Fill;

            gbInclinacao.Controls.Add(panelBotoesInclinacao);

            Button btnElevar = new Button();            
            btnElevar.Image = Resources.btnElevar;
            btnElevar.Size = btnElevar.Image.Size;
            btnElevar.Click += (sender, e) => ElevarSensorKinect(sender, e, sensor);

            Button btnBaixar = new Button();
            btnBaixar.Image = Resources.btnBaixar;
            btnBaixar.Size = btnBaixar.Image.Size;
            btnBaixar.Click += (sender, e) => BaixarSensorKinect(sender, e, sensor);

            Button btnCentralizar = new Button();
            btnCentralizar.Image = Resources.btnCentralizarSensor;
            btnCentralizar.Size = btnCentralizar.Image.Size;
            btnCentralizar.Click += (sender, e) => ResetarPosicaoKinect(sender, e, sensor);

            panelBotoesInclinacao.Controls.Add(btnElevar);
            panelBotoesInclinacao.Controls.Add(btnCentralizar);
            panelBotoesInclinacao.Controls.Add(btnBaixar);

            GroupBox gbBotoesUsuario = new GroupBox();
            gbBotoesUsuario.Text = "Serviços";
            gbBotoesUsuario.Dock = DockStyle.Fill;
            gbBotoesUsuario.MaximumSize = new Size(70, 190);
            tableGroupboxesInclinacaoFuncionalidades.Controls.Add(gbBotoesUsuario, 1, 0);

            FlowLayoutPanel panelBotoesFuncionalidades = new FlowLayoutPanel();
            panelBotoesFuncionalidades.FlowDirection = FlowDirection.TopDown;
            panelBotoesFuncionalidades.Dock = DockStyle.Fill;

            gbBotoesUsuario.Controls.Add(panelBotoesFuncionalidades);
            
            //modo diurno é o padrão (checked= false)
            CheckBox cbModoDiurnoNoturno = new CheckBox();
            cbModoDiurnoNoturno.Appearance = Appearance.Button;
            cbModoDiurnoNoturno.Checked = false;
            cbModoDiurnoNoturno.Image = Resources.btnModoDiurno;
            cbModoDiurnoNoturno.Size = cbModoDiurnoNoturno.Image.Size;
            cbModoDiurnoNoturno.CheckedChanged += (sender, e) => AlternarModoDiurnoNoturno(sender, e, sensor);

            Button btMobiles = new Button();
            btMobiles.Image = Resources.btnMobile;
            btMobiles.Size = Resources.btnMobile.Size;

            //checked: true indica rastreamento ativado, valor inicial
            CheckBox cbRastreamento = new CheckBox();
            cbRastreamento.Appearance = Appearance.Button;
            cbRastreamento.Checked = true;
            cbRastreamento.Image = Resources.btnRastreamentoAtivado;
            cbRastreamento.Size = cbModoDiurnoNoturno.Image.Size;
            cbRastreamento.CheckedChanged += (sender, e) => AlternarRastreamento(sender, e, sensor);

            panelBotoesFuncionalidades.Controls.Add(cbModoDiurnoNoturno);
            panelBotoesFuncionalidades.Controls.Add(cbRastreamento);
            panelBotoesFuncionalidades.Controls.Add(btMobiles);

            kinectSensor = sensor;
            SetupSensorVideoInput(sensor, ColorImageFormat.RgbResolution640x480Fps30);
            panelPai.Controls.Add(tablePanel);
        }

        private void AlternarRastreamento(object sender, EventArgs e, KinectSensor sensor)
        {
            var checkBox = sender as CheckBox;
            FileStream fileStream = null;
            string arquivo = sensor.UniqueKinectId.Substring(22) + ".txt";
            if (!checkBox.Checked)
            {
                var confirmacao = MessageBox.Show("Ao desativar o rastreamento, o sistema não enviará notificações " +
                "até que seja novamente reativado." + Environment.NewLine +
                "Só execute essa operação caso haja necessidade." +
                Environment.NewLine +
                "Deseja REALMENTE desativar? Essa ação irá gerar um registro para fins de controle.",
                "ATENÇÃO", MessageBoxButtons.YesNo);
                if (confirmacao == DialogResult.Yes)
                {
                    if (!File.Exists(arquivo))
                    {
                        fileStream = File.Open(arquivo, FileMode.Create);
                        fileStream.Close();
                    }
                    File.AppendAllText(arquivo, "Rastreamento do sensor " + sensor.UniqueKinectId + 
                        " desativado em: " + DateTime.Now.ToString()
                            + Environment.NewLine);
                    
                    checkBox.Image = Resources.btnRastreamentoDesativado;
                    DesativarRastreamento(sensor);                    
                }
            }
            else
            {
                if (!File.Exists(arquivo))
                {
                    fileStream = File.Create(arquivo);
                    fileStream.Close();
                }
                File.AppendAllText(arquivo, "Rastreamento do sensor " + sensor.UniqueKinectId + 
                    " reativado em: " + DateTime.Now.ToString()
                    + Environment.NewLine);
                
                checkBox.Image = Resources.btnRastreamentoAtivado;
                AtivarRastreamento(sensor);
            }            
        }

        private void AlternarModoDiurnoNoturno(object sender, EventArgs e, KinectSensor sensor)
        {
            var checkBox = sender as CheckBox;
            if (!checkBox.Checked)
            {
                checkBox.Image = Resources.btnModoDiurno;
                //sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                SetupSensorVideoInput(sensor, ColorImageFormat.RgbResolution640x480Fps30);
            }
            else
            {
                checkBox.Image = Resources.btnModoNoturno;
                SetupSensorVideoInput(sensor, ColorImageFormat.InfraredResolution640x480Fps30);
            }
        }

        private void ElevarSensorKinect(object sender, EventArgs e, KinectSensor sensor)
        {
            try
            {
                if ((sensor.ElevationAngle + 5) < sensor.MaxElevationAngle)
                    sensor.ElevationAngle += 5;
                else
                    MessageBox.Show("O sensor já está em seu limite máximo de elevação.", "Limite atingido");
            }
            catch
            {
                MessageBox.Show("O sensor já está em seu limite máximo de elevação.", "Erro");
            }
        }

        private void BaixarSensorKinect(object sender, EventArgs e, KinectSensor sensor)
        {
            try
            {
                if ((sensor.ElevationAngle - 5) > sensor.MinElevationAngle)
                    sensor.ElevationAngle -= 5;
                else
                    MessageBox.Show("O sensor já está em seu limite mínimo de elevação.", "Limite atingido");
            }
            catch
            {
                MessageBox.Show("O sensor já está em seu limite mínimo de elevação.", "Erro");
            }
        }

        private void ResetarPosicaoKinect(object sender, EventArgs e, KinectSensor sensor) 
        {
            sensor.ElevationAngle = 0;        
        }

        private Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();

                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }
    }
}
